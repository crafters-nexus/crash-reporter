import json
import logging
import os
import shutil
from datetime import datetime, timezone

from discord_webhook import DiscordEmbed, DiscordWebhook
from github import Github, InputFileContent

__location__ = os.path.realpath(os.path.join(os.getcwd(),
                                os.path.dirname(__file__)))

# Logging configuration
logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',
                    filename="crash_reporter.log",
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.INFO)

start_of_check = "-" * 15 + " START OF CHECK " + "-" * 15
end_of_check = "-" * 15 + " END OF CHECK " + "-" * 15


class CrashReporter:
    def __init__(self):
        logging.info(start_of_check)
        logging.info("Loading configuration...")
        with open(os.path.join(__location__, "crash_reports.json"), "r") as f:
            self.cache = json.load(f)
            f.close()
        self.config = self.cache.get("config")
        if self.config is None:
            logging.error("Configuration was not found! Something is wrong with the setup.")
            logging.error(end_of_check)
            exit()
        logging.info("Collecting a list of crash reports...")
        self.crash_reports = [report for report in os.listdir(os.path.join(__location__, "crash-reports")) if os.path.splitext(report)[1] == ".txt"]
        # No reports to process, just exit
        if len(self.crash_reports) == 0:
            logging.info("No crash reports to report.")
            logging.info(end_of_check)
            exit()
        token = self.config.get("token")
        url = self.config.get("webhook_url")
        server_name = self.config.get("server_name") or "Empty Server Name"
        if url is None:
            logging.error("URL in configuration was not found. Please double check config!")
            logging.error(end_of_check)
            exit()
        if token is None:
            logging.error("GitHub token is not present!")
            logging.error(end_of_check)
            exit()
        # Try to make the reported directory so we can move reports we handled.
        self.reported_directory = os.path.join(__location__, "crash-reports", "reported")
        try:
            os.mkdir(self.reported_directory, mode=0o777)
            logging.info("Created the directory to store reported crashes in: {}".format(self.reported_directory))
        except FileExistsError:
            pass
        self.webhook_url = url
        self.server = server_name
        logging.info("Connecting to github using token...")
        self.github = Github(token)
        self.user = self.github.get_user()
    
    def save_cache(self):
        cache_file = open(os.path.join(__location__, "crash_reports.json"), "w+")
        json.dump(self.cache, cache_file, indent=4)
        cache_file.close()
        
    def create_gist(self, file_name, file_contents):
        logging.info("Creating a gist for file: {}".format(file_name))
        gist_file = {file_name: InputFileContent(file_contents)}
        gist = self.user.create_gist(public=False, files=gist_file, description="{} crashed!".format(self.server))
        return gist.html_url
    
    def post_to_webhook(self, gist_url, report_time):
        logging.info("Posting the gist to the webhook...")
        dt = datetime.fromtimestamp(report_time)
        tz_string = datetime.now().replace(tzinfo=timezone.utc).astimezone(tz=None).strftime("%Z")
        formatted_time = dt.strftime("%b %d, %Y at %I:%M %p {}".format(tz_string))
        # Make a new webhook
        webhook = DiscordWebhook(url=self.webhook_url, username=self.server)
        # Create and embed and set time stamp for the embed, add it to the webhook and execute it.
        embed = DiscordEmbed(title="{} crashed!".format(self.server))
        embed.add_embed_field(name="Report URL", value="Read the report [here]({})".format(gist_url), inline=False)
        embed.add_embed_field(name="Report time", value=formatted_time, inline=False)
        webhook.add_embed(embed)
        return webhook.execute()
        
    def handle_crash_report(self):
        # Queue of new files to add to cache
        cache_queue = []
        # List of reports we've processed
        reports = [temp for temp in self.cache["reports"]]
        for report_name in self.crash_reports:
            # If it's not a report we've processed, it's a new report to update
            if report_name not in reports:
                # Get the report's path, read the file and get it's modified time
                report_path = os.path.join(__location__, "crash-reports", report_name)
                report = open(report_path).read()
                report_time = os.path.getmtime(report_path)
                
                # Create a gist and post to the webhook
                gist_url = self.create_gist(report_name, report)
                response = self.post_to_webhook(gist_url, report_time)
                status_code = response.status_code
                logging.info("Webhook triggered for file {}. Status code: {}".format(report_name, status_code))
                # If the status code is a 204, it worked
                if status_code == 204:
                    # Add the report to our cache queue and then move the file to the reported directory.
                    cache_queue.append(report_name)
                    new_path = os.path.join(self.reported_directory, report_name)
                    shutil.move(report_path, new_path)
                else:
                    message = "There was an error when executing the webhook event for crash report: {}\nGist URL: {}\n"
                    logging.error(message.format(report_name, gist_url))
                    logging.error("Response: {}".format(response.__dict__))
                    exit()
                        
        if len(cache_queue) > 0:
            for item in cache_queue:
                self.cache["reports"].append(item)
            self.save_cache()
        logging.info(end_of_check)


if __name__ == "__main__":
    reporter = CrashReporter()
    reporter.handle_crash_report()
