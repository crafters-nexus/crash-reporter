# Minecraft Crash Reporter

A potential candidate for Minecraft crash reporter to discord

## Installation

**Before continuing:**

Make sure you have Python 3 installed:

1. `python3 -V` or `python -V` depending on how it's configured. You should have: `Python 3.6` or newer preferably

**Empty the `crash-reports` folder or else you will spam the channel when it first runs!**

Only once the above things are complete and satisfied, we can continue one with installation.

NOTE: All `python` commands should be substituted with `python3` if the `python` command points to python 2.7 for you! 

1. `python -m pip install -r requirements.txt`

2. Place a copy of the script and `TEMPLATE_crash_reports.json` in all of the server's root directories (where forge and the server JARs are)

3. Edit the `TEMPLATE_crash_reports.json` file's `config` section to have the server's name, the [Discord webhook URL](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) you are using for that server and the GitHub token. You can create a GitHub token in your account settings here [here](https://github.com/settings/tokens). Once done, remove `TEMPLATE_` from the file name so it's just `crash_reports.json`

4. Add the following line to the server's `scripts.txt` file and format to fit what's in that file:

`@serverstopped $python crash_reporter.py`

And you should be set! Next time the server stops, it will go over all `txt` files in the `crash-reports` folder and if there are new ones, it will upload it to GitHub gist and post a message to the webhook with the link to the gist.
